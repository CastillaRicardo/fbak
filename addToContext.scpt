on run {input, parameters}
	set filesString to ""
	set currFolder to ""
	repeat with file_ in input
		set filesString to filesString & " " & quoted form of (POSIX path of file_)
	end repeat
	tell application "System Events"
		set currFolder to POSIX path of (container of (item 1 of input))
	end tell
	--display dialog currFolder
	set command to ("/usr/local/bin/fbak -n ") & currFolder & filesString
	set returnValue to (do shell script command) as number
	if returnValue is 0 then
		display notification "All good!"
	else
		display notification "There were problems :("
	end if
	return input
end run
