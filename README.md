# fbak #
Simple UNIX/Linux backup script

**Usage:**
```
$ fbak [-h] [-L] [-n <current_directory_on_gui>] [-m <move_to_dir> ] TARGET
```
*Targets can be directories, or files.*
*Feel free to use wildcards*

**Installation:**

Open a terminal and run

```
$ ./install.run
```
*Remember to use sudo if necessary*

**Adding to OS X context menu**

1. Open Automator.app
2. Create Service
3. Under Utilities, Select/Drag 'Run AppleScript'
4. In the space provided, copy and paste the code in addToContext.scpt
5. In the dropdown titled 'Service receives selected' choose 'files or folders'
6. Save the workflow with a descriptive name (e.g Run fbak)

You should now have an entry on the context menu (or under the Services submenu) with the name you gave the workflow

**Adding to Linux context menu**

You will have to follow your desktop environment's action creation guide.

**XFCE:**

1. Open your file manager
2. Select `Edit` tab
3. Select `Configure custom actions`
4. Give it a descriptive name (e.g. Run fbak) and a description
5. If you ran the install script, your command will look like `fbak -n %D %F`. If not, you will have to specifiy the absolute path to fbak
6. Under `Appearance and Conditions`, make sure all selection options are checked and file pattern is *
