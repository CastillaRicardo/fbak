fbak
====
Simple UNIX/Linux backup script

=============
Usage:
=============
$ fbak [-h] [-m <move_to_dir> ] TARGET

*Targets can be directories, or files.
*Feel free to use wildcards

=============
Installation:
=============
Open a terminal and run
```
$ ./install.run
```
*Remember to use sudo if necessary*

=============
Adding to OS X context menu
=============
1. Open Automator.app
2. Create Service
3. Under Utilities, Select/Drag 'Run AppleScript'
4. In the space provided, copy and paste the code in addToContext.scpt
5. In the dropdown titled 'Service receives selected' choose 'files or folders'
6. Save the workflow with a descriptive name (e.g Run fbak)

You should now have an entry on the context menu (or under the Services submenu) with the name you gave the workflow

=============
Adding to Linux context menu
=============
You will have to follow your desktop environment's action creation guide.

**XFCE:**
*Open your file manager
*Select `Edit` tab
*Select `Configure custom actions`
*Give it a descriptive name (e.g. Run fbak) and a description
*If you ran the install script, your command will look like `fbak -n %D %F`. If not, you will have to specifiy the absolute path to fbak
*Under `Appearance and Conditions`, make sure all selection options are checked and file pattern is `*`

**GNOME:**
*Use nautilus-actions
